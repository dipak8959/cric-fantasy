/* eslint-disable */
import React from "react";
import {
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
} from "@material-ui/core";
import { capitalize } from "lodash";

// components
import { Button } from "../../../../components/Wrappers";

// Icons
import * as Icons from "@material-ui/icons";

const states = {
  sent: "success",
  pending: "warning",
  declined: "secondary",
};

const keys = ['rank', 'team name','points', 'Immediate LEAD'];
const getRealName = (userId) => {
  let userName = '';
  switch (userId) {
    case "29186727":
      userName = 'Vikas'
      break;  
    case "67800493":
      userName = 'Mukesh'
      break;  
    case "68372400":
      userName = 'Vipul'
      break;  
    case "99889785":
      userName = 'Vivek'
      break;  
    case "68079480":
      userName = 'Rupam'
      break;  
    case "100520743":
      userName = 'Phani'
      break;  
    case "100503783":
      userName = 'Arun'
      break;  
    case "37054202":
      userName = 'Kalyan'
      break;  
    case "83291783":
      userName = 'Abhigyan'
      break;  
    case "67990085":
      userName = 'Ravi'
      break;  
    case "99272963":
      userName = 'Sudharsan'
      break;  
    case "68088373":
      userName = 'Kamesh'
      break;  
    case "99286719":
      userName = 'Dipak'
      break;                           
    case "237155":
      userName = 'Sathish'
      break;
    case "100442865":
      userName = 'Shanmuka'
      break;                               
    case "99208503":
      userName = 'Karthik'
      break;                               
    case "23847324":
      userName = 'Shivan'
      break;                               
    case "99109900":
      userName = 'Abhi'
      break;                               
      
    default:
      break;
  }
  return userName;
};
const getPreviousRankDiff = (rank, prevRanking) => {
  const rankingDifference = prevRanking?.rank - rank;
  if(rankingDifference === 0){
    return <><img style={{width:'16px',height:'16px', marginLeft: '18px'}} src="https://cdn3.iconfinder.com/data/icons/iconano-web-stuff/512/015-Sort-H-512.png"/></>
  }
  if(rankingDifference > 0){
    return <div style ={{marginLeft: '18px', color: 'green'}}>{`+${rankingDifference}`}</div>;
  }
  return <div style ={{marginLeft: '18px', color: 'red'}}>{rankingDifference || '--'}</div>;
}

const getDiff = (usrscoid, data) => {
  const userIndex = data.findIndex(user=> user.usrscoid === usrscoid);
  if(userIndex === 0){
    return `+${data[0].points - data[1].points}`
  }
  else if(userIndex === data.length-1)
  {
    return `NA`;
  }
  else{
    return `+${data[userIndex].points - data[userIndex+1].points}`;
  }
}

export default function TableComponent({ data, prevRankingData }) {
  return (
    <Table className="mb-0">
      <TableHead>
        <TableRow>
          {keys.map(key => (
            <TableCell key={key}>{capitalize(key)}</TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {data.map(({ temid, rank, usrscoid, temname, points }) => (
          <TableRow key={temid}>
            <TableCell className="pl-3 fw-normal"><div style={{display: 'flex', alignItems: 'center', flexDirection: 'row'}}><div style={{fontWeight:'bold', width: '14px'}}>{rank}</div>{getPreviousRankDiff(rank,prevRankingData.find(x=>x.usrscoid === usrscoid))}</div></TableCell>                        
            <TableCell>
              <div style={{display:'flex', flexDirection:'column'}}>
                <div style={{display:'flex', flexDirection:'row'}}>
                  {capitalize(temname)}
                </div>
                <div style={{ paddingBottom: '10px'}}/>
                <div style={{display:'flex', flexDirection:'row', fontSize:'10.5px', color:'#6E6E6E'}}>
                [{capitalize(getRealName(usrscoid))}]
                </div>
              </div>
             </TableCell>
            <TableCell>{points}</TableCell>
            <TableCell>{getDiff(usrscoid, data)}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}
