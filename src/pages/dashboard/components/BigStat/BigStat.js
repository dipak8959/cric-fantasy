/* eslint-disable */
import React, { useState } from "react";
import { Grid, Select, MenuItem, Input } from "@material-ui/core";
import { ArrowUpward } from "@material-ui/icons";
import { useTheme } from "@material-ui/styles";
import { BarChart, Bar } from "recharts";
import classnames from "classnames";

// styles
import useStyles from "./styles";

// components
import Widget from "../../../../components/Widget";
import { Typography } from "../../../../components/Wrappers";

export default function BigStat(props) {
  var { title, data } = props;
  var classes = useStyles();
  var theme = useTheme();

  // local
  var [value, setValue] = useState("daily");

  return (
    <Widget
      header={
        <div className={classes.title}>
          <Typography variant="h5">{title}</Typography>
        </div>
      }
      upperTitle
    >
      <div className={classes.totalValueContainer}>
        <div className={classes.totalValue} style={{display:'flex'}}>
          <Typography size="md" color="text" colorBrightness="secondary">
          {data.points}
          </Typography>
          <div className={classnames(classes.statCell, classes.borderRight)}>
          <Grid container alignItems="center">
          <Typography variant="h8">{data.usrname}</Typography>
          </Grid>
        </div>

        </div>
      </div>
      <div className={classes.bottomStatsContainer}>
      </div>
    </Widget>
  );
}

// #######################################################################

function getRandomData() {
  return Array(7)
    .fill()
    .map(() => ({ value: Math.floor(Math.random() * 10) + 1 }));
}
