export default {
   "Data":{
      "Value":[
         {
            "rno":1,
            "temid":48590206,
            "temname":"INFONIRMAL",
            "usrname":"INFONIRMAL",
            "usrscoid":"67800493",
            "usrclnid":1,
            "rank":1,
            "points":10414.5,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":2,
            "temid":677500207,
            "temname":"VIVEK CHARGERS 5787",
            "usrname":"VIVEK CHARGERS 5787",
            "usrscoid":"99889785",
            "usrclnid":1,
            "rank":2,
            "points":9805.0,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":3,
            "temid":125890404,
            "temname":"Team TopGuns",
            "usrname":"Team TopGuns",
            "usrscoid":"29186727",
            "usrclnid":1,
            "rank":3,
            "points":9767.0,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":4,
            "temid":46940104,
            "temname":"UnsulliedWithBalls",
            "usrname":"UnsulliedWithBalls",
            "usrscoid":"68079480",
            "usrclnid":1,
            "rank":4,
            "points":9740.5,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":5,
            "temid":49850201,
            "temname":"Legend2K",
            "usrname":"Legend2K",
            "usrscoid":"67990085",
            "usrclnid":1,
            "rank":5,
            "points":9638.5,
            "ftgdid":1,
            "prfimg":"https://userassets.dream11.com/fbprofilepic/53a03920d594be4218fb6b891fd023ee1f48b0cb_profilepic.jpg?id=3a7340d0-f9b5-11ea-9cea-1dfa1b50024b"
         },
         {
            "rno":6,
            "temid":126710110,
            "temname":"Chowk1dar",
            "usrname":"Chowk1dar",
            "usrscoid":"68372400",
            "usrclnid":1,
            "rank":6,
            "points":9524.0,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":7,
            "temid":973510101,
            "temname":"404  Team Not Found",
            "usrname":"404  Team Not Found",
            "usrscoid":"23847324",
            "usrclnid":1,
            "rank":7,
            "points":9482.0,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":8,
            "temid":42570406,
            "temname":"Cult of Kattappa",
            "usrname":"Cult of Kattappa",
            "usrscoid":"99272963",
            "usrclnid":1,
            "rank":8,
            "points":9415.0,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":9,
            "temid":1132420410,
            "temname":"deccanchargers97",
            "usrname":"deccanchargers97",
            "usrscoid":"83291783",
            "usrclnid":1,
            "rank":9,
            "points":9407.0,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":10,
            "temid":43740305,
            "temname":"Chindi Chor XI",
            "usrname":"Chindi Chor XI",
            "usrscoid":"37054202",
            "usrclnid":1,
            "rank":10,
            "points":9246.5,
            "ftgdid":1,
            "prfimg":"https://userassets.dream11.com/fbprofilepic/0171f57fed1eb7bddd2b2e2907259338de21e535_profilepic.jpg?id=f4d0e1c0-f93e-11ea-8ae9-bf8999b071ca"
         },
         {
            "rno":11,
            "temid":1146850403,
            "temname":"Swansong",
            "usrname":"Swansong",
            "usrscoid":"100503783",
            "usrclnid":1,
            "rank":11,
            "points":9206.5,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":12,
            "temid":69190402,
            "temname":"SatXII",
            "usrname":"SatXII",
            "usrscoid":"237155",
            "usrclnid":1,
            "rank":12,
            "points":9123.0,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":13,
            "temid":45010407,
            "temname":"WhistIe Podu",
            "usrname":"WhistIe Podu",
            "usrscoid":"99286719",
            "usrclnid":1,
            "rank":13,
            "points":8898.5,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":14,
            "temid":599550202,
            "temname":"cyberwolfXI",
            "usrname":"cyberwolfXI",
            "usrscoid":"68088373",
            "usrclnid":1,
            "rank":14,
            "points":8747.5,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":15,
            "temid":180104,
            "temname":"Katte Bat XI",
            "usrname":"Katte Bat XI",
            "usrscoid":"99109900",
            "usrclnid":1,
            "rank":15,
            "points":8603.5,
            "ftgdid":1,
            "prfimg":"https://userassets.dream11.com/fbprofilepic/f115e4bec97baf8b29b2a94890acfe1632a0cb84_profilepic.jpg?id=1121ddc0-f1dd-11ea-8085-a062120327b4"
         },
         {
            "rno":16,
            "temid":1101760208,
            "temname":"ChichasX1",
            "usrname":"ChichasX1",
            "usrscoid":"100442865",
            "usrclnid":1,
            "rank":16,
            "points":8552.5,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":17,
            "temid":1154460410,
            "temname":"Guntur gangsta",
            "usrname":"Guntur gangsta",
            "usrscoid":"100520743",
            "usrclnid":1,
            "rank":17,
            "points":8148.0,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         },
         {
            "rno":18,
            "temid":26390406,
            "temname":"KARTHIK EAGLES 4062",
            "usrname":"KARTHIK EAGLES 4062",
            "usrscoid":"99208503",
            "usrclnid":1,
            "rank":18,
            "points":7898.0,
            "ftgdid":1,
            "prfimg":"https://www.dream11.com/public/imgs/leaderboard_default_image.png"
         }
      ],
      "FeedTime":{
         "UTCTime":"10/23/20 12:44:13 PM",
         "ISTTime":"10/23/20 6:14:13 PM",
         "CESTTime":"10/23/20 2:44:13 PM"
      }
   },
   "Meta":{
      "Message":"cf_user_league_rank_get",
      "RetVal":1,
      "Success":true,
      "Timestamp":{
         "UTCTime":"10/23/20 12:44:13 PM",
         "ISTTime":"10/23/20 6:14:13 PM",
         "CESTTime":"10/23/20 2:44:13 PM"
      }
   }
}